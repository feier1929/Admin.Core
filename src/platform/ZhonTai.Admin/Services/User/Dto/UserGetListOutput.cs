﻿namespace ZhonTai.Admin.Services.User.Dto;

public class UserGetListOutput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }
}