﻿namespace ZhonTai.Admin.Domain.Role;

/// <summary>
/// 角色类型
/// </summary>
public enum RoleTypeEnum
{
    /// <summary>
    /// 自定义
    /// </summary>
    Custom = 0,

    /// <summary>
    /// 默认
    /// </summary>
    Default = 1,

    /// <summary>
    /// 主管理员
    /// </summary>
    MainAdmin = 101,
}
