﻿namespace ZhonTai.Admin.Domain.User.Dto;

public partial class UserGetListInput
{
    /// <summary>
    /// 姓名
    /// </summary>
    public string Name { get; set; }
}